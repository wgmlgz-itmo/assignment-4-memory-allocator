#include <assert.h>
#include <stdio.h>

#include "mem.h"

#define RUN_TEST(testFunc)               \
  do {                                   \
    printf("Running %s... ", #testFunc); \
    testFunc();                          \
    printf("Test OK\n");                 \
  } while (0)

void test_successful_allocation() {
  void* block1 = _malloc(100);
  assert(block1 != NULL);
  _free(block1);
}

void test_freeing_single_block() {
  void* block1 = _malloc(100);
  void* block2 = _malloc(200);
  _free(block1);
  _free(block2);
}

void test_freeing_two_blocks() {
  void* block1 = _malloc(100);
  void* block2 = _malloc(200);
  _free(block1);
  _free(block2);
}

void test_memory_exhaustion_extend_region() {
  void* block1 = _malloc(100000);
  void* block2 = _malloc(50000);
  assert(block2 != NULL);
  _free(block1);
  _free(block2);
}

void test_memory_exhaustion_new_region() {
  void* block = NULL;
  size_t size = 100000;
  do {
    block = _malloc(size);
    if (block) {
      _free(block);
      size += 100000;
    }
  } while (block);
}

int main() {
  heap_init(1024 * 1024);

  RUN_TEST(test_successful_allocation);
  RUN_TEST(test_freeing_single_block);
  RUN_TEST(test_freeing_two_blocks);
  RUN_TEST(test_memory_exhaustion_extend_region);
  RUN_TEST(test_memory_exhaustion_new_region);

  heap_term();
  return 0;
}
